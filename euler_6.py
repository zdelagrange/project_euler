def sum_of_squares(my_nums):
	my_sum = 0
	for num in my_nums:
		my_sum += num ** 2
	return my_sum

def square_of_sum(my_nums):
	winner = 0
	for num in my_nums:
		winner += num
	return winner ** 2

if __name__== "__main__":
	print(square_of_sum(range(1,101)) - sum_of_squares(range(1,101)))
