from pprint import pprint


def test_pythargean_triple(a, b ,c):

    if (a ** 2 + b ** 2) == (c ** 2):
        return True
    else:
        return False


def test_addition_equals_one_thousand(a, b, c):
    if a + b + c == 1000:
        return True
    else:
        return False


def test_c_greatest(a, b, c):
    if a < c and b < c:
        return True
    else:
        return False


def generate_three_values_sum_one_thousand():
    x = range(1, 1000)
    y = range(1, 1000)
    z = range(1, 1000)

    woot = []
    i = 0
    for num_x in x:
        for num_y in y:
            for num_z in z:
                if test_addition_equals_one_thousand(num_x, num_y, num_z) and remove_non_pythagorean_triple([num_x, num_y, num_z]):
                    print("WINNER!!!")
                    print([num_x, num_y, num_z])
                    woot.append([num_x, num_y, num_z])
                else:
                    print([num_x, num_y, num_z])
                    print("nope " + str(i))
                i += 1
    return woot


def remove_non_pythagorean_triple(triple):
    triple.sort()
    if not test_c_greatest(triple[0], triple[1], triple[2]):
        return False
    elif not test_pythargean_triple(triple[0], triple[1], triple[2]):
        return False
    return True


if __name__ == "__main__":
    my_list = generate_three_values_sum_one_thousand()
    pprint(my_list)
