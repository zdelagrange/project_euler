def is_prime(n):
    if n <= 3:
        return n >= 2
    if n % 2 == 0 or n % 3 == 0:
        return False
    for i in range(5, int(n ** 0.5) + 1, 6):
        if n % i == 0 or n % (i + 2) == 0:
            return False
    return True

if __name__ == "__main__":
	num = 2
	num_of_primes = 0
	while num_of_primes < 10001:
		if is_prime(num):
			num_of_primes += 1
		num += 1
	print num
	print num_of_primes

