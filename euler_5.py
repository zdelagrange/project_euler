def is_evenly_divisible_by_one_to_twenty():
	start_num = 2520
	divisible_numbers = range(2,21)
	divisible_numbers.reverse()
	is_found = False
	while not is_found:
		for number  in divisible_numbers:
			if start_num % number != 0:
				start_num += 1
				break
			elif number == divisible_numbers[-1] and start_num % number == 0:
				print "success! {0}".format(start_num)

if __name__ == "__main__":
	is_evenly_divisible_by_one_to_twenty()
